import Game from './Game'
import config from './config/Game'

window.onload = () => {
  const GameObject = new Game(config)
  GameObject.log()
}
