import Main from '../scenes/Main'
import Phaser from 'phaser'

const Game = {
  type: Phaser.AUTO,
  width: 480,
  height: 640,
  parent: 'phaser-game',
  scene: [Main]
}

export default Game
