export default class Align {
  static scaleToGameWidth (object, percentage) {
    object.setDisplaySize(object.scene.game.config.width * percentage)
    object.setScale(object.scaleX)
    return object
  }

  static center (object) {
    this.centerX(object)
    this.centerY(object)
    return object
  }

  static centerX (object) {
    object.setX(object.scene.game.config.width / 2)
    return object
  }

  static centerY (object) {
    object.setY(object.scene.game.config.height / 2)
    return object
  }
}
