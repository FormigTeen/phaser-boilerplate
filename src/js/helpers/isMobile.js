const isMobile = () => navigator.userAgent.indexOf('Mobile') >= 0

export default isMobile
