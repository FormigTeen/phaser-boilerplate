export default class Grid {
  constructor ({ scene, rows, cols }) {
    this.scene = scene
    this.rows = rows
    this.cols = cols
    this.width = this.scene.game.config.width
    this.height = this.scene.game.config.height
    this.vRow = this.height / this.rows
    this.vCol = this.width / this.cols
    this.graphics = this.scene.add.graphics()
    this.graphics.lineStyle(2, 0xff0000)
    this.draw()
  }

  drawCols () {
    for (let i = 0; i < this.width; i += this.vCol) {
      this.graphics.moveTo(i, 0)
      this.graphics.lineTo(i, this.height)
    }
  }

  drawRows () {
    for (let i = 0; i < this.height; i += this.vRow) {
      this.graphics.moveTo(0, i)
      this.graphics.lineTo(this.width, i)
    }
  }

  drawNumbers () {
    for (let i = 1; i <= this.cols; i++) {
      for (let j = 1; j <= this.rows; j++) {
        const index = (j - 1) * this.cols + i
        const text = this.scene.add.text(0, 0, index).setOrigin(0.5, 0.5)
        this.putAtIndex(text, index)
      }
    }
  }

  draw () {
    this.drawRows()
    this.drawCols()
    this.graphics.strokePath()
    this.drawNumbers()
  }

  putOnRow (object, number) {
    object.setY(this.vRow * number)
    return this
  }

  putOnCol (object, number) {
    object.setX(this.vCol * number)
    return this
  }

  putOnGrid (object, col, row) {
    this.putOnCol(object, col).putOnRow(object, row)
  }

  putAtIndex (object, index) {
    const indexOnX = (index - 1) % this.cols
    const indexOnY = ((index - 1) - ((index - 1) % this.cols)) / this.cols
    object.setPosition((this.vCol * indexOnX) + (this.vCol / 2), (this.vRow * indexOnY) + (this.vRow / 2))
    return this
  }
}
